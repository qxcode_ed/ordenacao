#include <iostream>
#include <queue>
#include <list>
#include <cstdlib>
#include <SFML/Graphics.hpp>
#include <queue>
using namespace std;

namespace prof{

int get_ind(int num, int ind){
    int resto;
    for(int i = 0; i <= ind; i++){
        resto = num % 10;
        num = num / 10;
    }
    return resto;
}

int get_dezena(int num, int indice){
    if(indice == 0)
        return num % 10;
    return get_dezena(num / 10, indice - 1);
}

int bucket(queue<int> vet){
    queue<int> numeros(vet);
    vector< queue<int> > baldes(10);


    for(int i = 0; i < 5; i++){
        while(!numeros.empty()){
            int num = numeros.front();
            numeros.pop();
            int ind = get_ind(num, i);
            baldes[ind].push(num);
        }
        for(int j = 0; j < 10; j++){
            while(!baldes[j].empty()){
                numeros.push(baldes[j].front());
                baldes[j].pop();
            }
        }
    }
    while(!numeros.empty()){
        cout << numeros.front() << " ";
        numeros.pop();
    }

    return 0;
}




}

//int main(){

//    srand(time(NULL));
//    queue<int> numeros;
//    for(int i = 0; i < 100; i++)
//        numeros.push(rand() % 10000);

//    prof::bucket(numeros);
//    return 0;
//}
