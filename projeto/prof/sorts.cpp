
#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <SFML/Graphics.hpp>

#include "../libs/sortview.h"

using namespace std;

static SortView sview;

namespace prof{


void selection_sort( vector<int> &vet ){
    for(int i = 0; i < (int)vet.size() - 1; i++){
        int jmin = i;
        for(int j = i + 1; j < (int)vet.size(); j++)
            if(vet[j] < vet[jmin])
                jmin = j;
        std::swap(vet[i], vet[jmin]);
        sview.show(vet, {i}, "r");
    }
}

//############################################################################
void bubble_sort(std::vector<int> &vet){
    int tam = vet.size();
    for(int i = tam - 1; i >= 1; i--) {
        for( int j = 0; j < i ; j++) {
            sview.show(vet, {j, j + 1, i}, "bbg");
            if(vet[j]>vet[j+1]) {
                std::swap(vet[j], vet[j + 1]);
                sview.show(vet, {j, j + 1, i}, "rrg");
            }
        }
    }
}
//############################################################################

//primeira particao [inicio, meio[
//segunda partica   [meio, fim[
void intercala(std::vector<int> &vet, int inicio, int meio, int fim){
    sview.set_faixa(inicio, fim - 1);
    int esq = inicio;
    int dir = meio;
    std::vector<int> temp;

    while(esq < meio && dir < fim){
        sview.show(vet, {inicio, esq, meio, dir, fim}, "bygmb");
        if(vet[esq] < vet[dir]){
            temp.push_back(vet[esq]);
            esq++;
            sview.show(vet, {inicio, esq, meio, dir, fim}, "bygmb");
        }else{
            temp.push_back(vet[dir]);
            dir++;
            sview.show(vet, {inicio, esq, meio, dir, fim}, "bygmb");
        }
    }
    while(esq < meio){
        temp.push_back(vet[esq]);
        esq++;
    }
    while(dir < fim){
        temp.push_back(vet[dir]);
        dir++;
    }

    for(int i = 0; i < (int)temp.size(); i++){
        vet[inicio + i] = temp[i];
    }

    sview.show(vet);
}

//inicio fechado até fim aberto
//[inicio, fim[
void merge_sort(std::vector<int> &vet, int inicio, int fim){
    if(fim - inicio > 1){
        int meio = (inicio + fim)/2;
        sview.show(vet, {inicio, meio, fim}, "bgb");
        merge_sort(vet, inicio, meio);
        merge_sort(vet, meio, fim);
        intercala(vet, inicio, meio, fim);
    }
}


//############################################################################
void quick_sort(std::vector<int> &vet, int esquerda, int direita)
{
    sview.set_faixa(esquerda, direita);
    sview.show(vet);
    int i, j, x;
    i = esquerda;
    j = direita;
    int pivo = esquerda;
    x = vet[pivo];

    //enquanto nao se cruzarem
    while(i <= j)
    {
        sview.show(vet, {i, j, pivo}, "ggr");
        //procura da esq pra direta alguem maior
        //desque nao cruze
        while(vet[i] < x && i < direita){
            i++;
            sview.show(vet, {i, j, pivo}, "ggr");
        }
        sview.show(vet, {i, j, pivo}, "mmr");
        //vem pela direita
        while(vet[j] > x && j > esquerda){
            j--;
            sview.show(vet, {i, j, pivo}, "mmr");
        }
        sview.show(vet, {i, j, pivo}, "yyr");
        //faz swap e anda
        if(i <= j)
        {
            sview.show(vet, {i, j, pivo}, "yyr");
            std::swap(vet[i], vet[j]);
            sview.show(vet, {i, j, pivo}, "rrr");
            i++;
            j--;
        }
    }
    if(j > esquerda)
        quick_sort(vet, esquerda, j);
    if(i < direita)
        quick_sort(vet,  i, direita);
    sview.show(vet);
}



//############################################################################

std::vector<int> rand_vet(int qtd, int min, int max){
    std::vector<int> vet;
    vet.reserve(qtd);
    for(int i = 0; i < qtd; i++){
        vet.push_back(rand() % (max - min + 1) + min);
    }
    return vet;
}


//i   : inicio da primeira parte
//meio: fim aberto da primeira parte
//meio: inicio da segunda parte
//j   : fim aberto da segunda parte
void meu_intercala(std::vector<int> &vet, int i, int meio, int j){
    std::list<int> left(vet.begin() + i, vet.begin() + meio);

    std::list<int> right(vet.begin() + meio, vet.begin() + j);
    auto it = vet.begin() + i;

//    while((left.size() > 0) && (right.size() > 0)){
//        if(left.front() < right.front()){
//            *it = left.front();
//            left.pop_front();
//        }else{
//            *it = right.front();
//            right.pop_front();
//        }
//        it++;
//    }

//    while(left.size() > 0){
//        *it = left.front();
//        it++;
//        left.pop_front();
//    }
//    while(right.size() > 0){
//        *it = right.front();
//        it++;
//        right.pop_front();
//    }

    while((left.size() > 0) && (right.size() > 0)){
        auto& lista = (left.front() < right.front()) ? left : right;
        *it = lista.front();
        lista.pop_front();
        it++;
    }
    it = std::copy(left.begin(), left.end(), it);
    it = std::copy(right.begin(), right.end(), it);
}

//i   : inicio da primeira parte
//meio: fim aberto da primeira parte
//meio: inicio da segunda parte
//j   : fim aberto da segunda parte
void meu_mergesort(std::vector<int> &vet, int i, int j){
    if(j - i > 1){//mais que um elemento entre i e j
        int meio = i + (j - i)/2;
        meu_mergesort(vet, i, meio);
        meu_mergesort(vet, meio, j);
        //meu_intercala(vet, i, meio, j);
        std::inplace_merge(vet.begin() + i,
                           vet.begin() + meio,
                           vet.begin() + j);
    }
    sview.show(vet);
}

int main_sorts(){
    srand(time(NULL));

    my_player->autoplay = false;


    sview.set_dot_view();
    sview.set_thickness(8);

    auto vet = rand_vet(70, 50, 300);

    std::random_shuffle(vet.begin(), vet.end());
    selection_sort(vet);

    std::random_shuffle(vet.begin(), vet.end());
    bubble_sort(vet);

    std::random_shuffle(vet.begin(), vet.end());
    quick_sort(vet, 0, vet.size() - 1);

    std::random_shuffle(vet.begin(), vet.end());
    merge_sort(vet, 0, vet.size());

    my_player->wait();
    return 0;
}
}//namespace profs

//int main(){
//    prof::main_sorts();
//}
